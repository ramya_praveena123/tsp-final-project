package com.ts;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.dao.AdminDao;
import com.dao.UserDao;
import com.model.Admin;
import com.model.User;

@CrossOrigin(origins = "http://localhost:4200")
@RestController
public class AdminController {

	@Autowired
	AdminDao adminDao;

	@GetMapping("getAdmin")
	public List<Admin> getAdmin() {
		return adminDao.getAdmin();
	}

	@GetMapping("getUserDetailsById/{UserId}")
	public Admin getUserDetailsById(@PathVariable int UserId) {
		return adminDao.getUserDetailsById(UserId);
	}

	@GetMapping("getUserDetailsByName/{UserName}")
	public Admin getUserDetailsByName(@PathVariable String UserName) {
		return adminDao.getUserDetailsByName(UserName);
	}

	@PutMapping("updateUserDetails")
	public Admin updateUserDetails(@RequestBody Admin Admin) {
		return adminDao.updateUserDetails(Admin);
	}

	@DeleteMapping("deleteUserDetailsById/{AdminId}")
	public String deleteUserDetailsById(@PathVariable int UserId) {
		adminDao.deleteUserDetailsById(UserId);
		return "Admin With AdminId:" + UserId + " Deleted Successfully!!!";
	}
	
	@GetMapping("adminLogin/{email}/{password}")
	public Admin UserLogin(@PathVariable String email, @PathVariable String password){
		return adminDao.adminLogin(email,password);
	}
	
}
















/*@PostMapping("addAdmin")
public Admin addAdmin(@RequestBody Admin Admin) {
	return adminDao.addUser(Admin);
}*/
