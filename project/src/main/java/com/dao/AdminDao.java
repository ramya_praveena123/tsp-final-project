package com.dao;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.model.Admin;
import com.model.User;

@Service
public class AdminDao {
    
    @Autowired
    private AdminRepository adminRepository;

    public List<Admin> getAdmin() {
        return adminRepository.findAll();
    }

    public Admin getUserDetailsById(int userId) {
        return adminRepository.findById(userId).orElse(null);
    }

    public Admin getUserDetailsByName(String userName) {
        return adminRepository.findByName(userName);
    }

    public Admin addUser(Admin user) {
        return adminRepository.save(user);
    }

    public Admin updateUserDetails(Admin user) {
        return adminRepository.save(user);
    }

    public void deleteUserDetailsById(int userId) {
        adminRepository.deleteById(userId);
    }

    public Admin adminLogin(String email, String password) {
        return adminRepository.adminLogin(email, password);
    }
}
