package com.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.model.Admin;
import com.model.User;


@Repository
public interface AdminRepository extends JpaRepository<Admin, Integer>{

	@Query("from Admin where uName = :userName")
	Admin findByName(@Param("userName") String userName);

	@Query("from Admin where email = :email and password = :password")
	Admin adminLogin(@Param("email") String email, @Param("password") String password);

	}

